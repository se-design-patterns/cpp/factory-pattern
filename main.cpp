#include "square.h"
#include "rectangle.h"
#include "circle.h"
#include "triangle.h"
#include "rhombus.h"

int main() 
{
    // Create Square
    shape* s = shape::create(square_shape);
    s->getShape();

    // Create Rectangle
    shape* r = shape::create(rectangle_shape);
    r->getShape();

    // Create Circle
    shape* c = shape::create(circle_shape);
    c->getShape();

    // Create Triangle
    shape* t = shape::create(triangle_shape);
    t->getShape();

    shape* rh = shape::create(rhombus_shape);
    rh->getShape();

    return 0;
}