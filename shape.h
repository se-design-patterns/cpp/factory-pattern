#pragma once

#include <string>
#include <iostream>

enum shapeType
{
    square_shape,
    rectangle_shape,
    circle_shape,
    triangle_shape,
    rhombus_shape
};

class shape
{
public:
    std::string type;
    virtual void getShape() = 0;
    static shape *create(shapeType type);
};