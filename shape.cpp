#include "shape.h"

#include "square.h"
#include "rectangle.h"
#include "circle.h"
#include "triangle.h"
#include "rhombus.h"

shape *shape::create(shapeType type)
{
    if (type == square_shape) 
    {
        return new square();
    }
    else if (type == rectangle_shape)
    {
        return new rectangle();
    }
    else if (type == circle_shape)
    {
        return new circle();
    }
    else if (type == triangle_shape)
    {
        return new triangle();
    }
    else if (type == rhombus_shape) 
    {
        return new rhombus();
    }
    else
    {
        return NULL;
    }
}